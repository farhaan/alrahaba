import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
// import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, MarkerOptions, LatLng } from '@ionic-native/google-maps';
import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, MarkerOptions, Environment, LatLng } from '@ionic-native/google-maps';

import { Platform, NavController, NavParams, ModalController } from 'ionic-angular';
// import { HomePage } from '../home/home';
import 'rxjs/add/operator/map';
declare var navigator: any;
@Component({
  selector: 'page-maps',
  templateUrl: 'maps.html',
})

export class MapsPage implements OnInit {
    isUo: boolean = false;
    isEa: boolean = false;
    isTo: boolean = false;
    busCheckbox: boolean = false;
    repCheckbox: boolean = false;
    tempLocation = [{ id: 1, name: 'IT-Office', date: "11-11-2018", lat: 21.517080, lng: 39.183231 },
    { id: 2, name: 'Head-Office', date: "12-11-2018", lat: 21.577827, lng: 39.167093 },
    { id: 3, name: 'Home', date: "13-11-2018", lat: 21.576718, lng: 39.169574 },
    { id: 4, name: 'Rakesh', date: '15-11-2018', lat: 16.6555572, lng: 74.5665 },
    { id: 4, name: ' Petrol Pump', lat: 16.6555572, lng: 74.5665 },
    { id: 4, name: 'Darga ', lat: 16.659353, lng: 74.569157 },
    { id: 4, name: 'Terwad  ', lat: 16.665593, lng: 74.574939 }];

    map: GoogleMap;
    watchPosition:any;

    
    latlang: LatLng;
    wathId: any;
    lat: any;
    lng: any;
    mapElement: HTMLElement;
    tracking: number;
    setAttributfor_mapLoad: string = 'UnLoaded'
    task: number;
    selfmarker: any;
    busbuttonColor: string;
    resbuttonColor: string = '#000';
    langugeKey: string;
    setDirection: string;
    getCurrentposition: any;
    mapArry: any[];
    addMar: boolean;
    constructor(public navCtrl: NavController, public platform: Platform, public navParams: NavParams, private geolocation: Geolocation) {        
        this.langugeKey = localStorage.getItem('lang')
        if (this.langugeKey && this.langugeKey == 'ar'){
            this.setDirection = 'rtl';
        }else{
            this.setDirection = 'ltr';
        }      
        this.busbuttonColor = '#f3f1f1'
        this.resbuttonColor = '#f3f1f1';
    }
    ngOnInit() {}
    ionViewDidLoad() {
        this.platform.ready().then(() => {
            this.loadMap();
        });
    }
    loadMap(){
        Environment.setEnv({
            'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyAzM5Y_FYGUcYhAsvoJez6biQ1wAvJZdG0',
            'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyAzM5Y_FYGUcYhAsvoJez6biQ1wAvJZdG0'
        });                
        let options = { timeout: 7000, enableHighAccuracy: true };
        this.geolocation.getCurrentPosition(options).then((resp) => {
            this.lat = resp.coords.latitude;
            this.lng = resp.coords.longitude;
            this.mapElement = document.getElementById('map');
            let mapOptions: GoogleMapOptions = { camera: { target: { lat: this.lat, lng: this.lng }, zoom: 18, tilt: 30 } };
            this.map = GoogleMaps.create(this.mapElement, mapOptions);
            this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
                console.log('google map is ready!...');
                this.startgeolocation();
            });
        });
    }

    startgeolocation(){
        let options = { frequency: 3000, enableHighAccuracy: true };
        this.wathId = this.geolocation.watchPosition(options).subscribe(resp => {
            this.lat = resp.coords.latitude;
            this.lng = resp.coords.longitude;
            let userPosition: LatLng = new LatLng(this.lat, this.lng);
            if (this.selfmarker != null) {
                this.selfmarker.setPosition(userPosition);
                this.map.addMarker(this.selfmarker);
            }else{
                let markerOptions: MarkerOptions = {
                    position: userPosition,
                    icon: 'red',
                    animation: 'BOUNCE'
                };
                this.map.addMarker(markerOptions).then((marker) => { this.selfmarker = marker; });
            }
            if(!this.busCheckbox && !this.repCheckbox){
                if(this.addMar == false){
                    var ZoomLocation = {
                        "lat": this.lat,
                        "lng": this.lng
                    }
                    this.map.moveCamera({
                        "target": ZoomLocation,
                        "tilt": 30,
                        "zoom": 18
                    });
                }
            }
        });
    }
    stopTrackingLoop() {
        clearInterval(this.tracking);
        this.tracking = null;
    }
    clearMap(){
        this.busCheckbox = false;
        this.repCheckbox = false;
        this.addMar = false;
        this.selfmarker = null
        this.busbuttonColor = '#f3f1f1'
        this.resbuttonColor = '#f3f1f1';
    }
    addmarkers(loc){
        this.addMar = true;    
        let isBUs = localStorage.getItem('isBus');
        let markerIcon;
        if (isBUs == 'true') {
            markerIcon = {
                'url': 'assets/imgs/movingBus.png',
                'size': { width: 40, height: 45, },
            }
        }else{
            markerIcon = {
                'url': 'assets/imgs/wmen_trans.png',
                'size': { width: 25, height: 40, }
            }
        }
        this.map.addMarker({
            title: loc.name,
            icon: markerIcon,
            animation: 'drop',
            position: {
                lat: loc.lat,
                lng: loc.lng
            }
        });
        var ZoomLocation = { "lat": loc.lat, "lng": loc.lng }
        this.map.moveCamera({
            "target": ZoomLocation,
            "tilt": 30,
            "zoom": 18
        });
        localStorage.setItem('lat',null);
    }

    showBus(){
        this.busCheckbox = !this.busCheckbox; this.addMar = false;
        if(this.busCheckbox){
            this.busbuttonColor = '#1471c1';
            this.mapArry = []
            this.mapArry = this.tempLocation;
            this.mapArry.forEach(res => {
                let markerIcon = {
                'url': 'assets/imgs/movingBus.png',
                'size': { width: 40, height: 45, },
                }
                this.map.addMarker({
                    title: 'Res Location',
                    icon: markerIcon,
                    animation: 'drop',
                    position: { lat: res.lat, lng: res.lng }
                });
            });
        }else{
            this.busbuttonColor = '#f3f1f1';
            this.map.clear();
        }
    }

    showRep(){
        this.repCheckbox = !this.repCheckbox; this.addMar = false
        if (this.repCheckbox){
            this.resbuttonColor = '#1471c1';
            var resloc = []
            resloc = this.tempLocation;
            resloc.forEach(res => {
                let markerIcon = {
                'url': 'assets/imgs/wmen_trans.png',
                'size': { width: 25, height: 40, }
                }
                this.map.addMarker({
                    title: 'Res Location',
                    icon: markerIcon,
                    animation: 'drop',
                    position: { lat: res.lat, lng: res.lng }
                });
            });
        }else{
            this.resbuttonColor = '#f3f1f1';
            if (!this.repCheckbox){
                this.map.clear();
                this.busCheckbox = !this.busCheckbox;
                this.showBus();
            }
        }
    }
 
    moveLocation() {
        this.clearMap();
        this.map.clear();
        if(this.map!=null){
          navigator.geolocation.clearWatch(this.wathId);
          this.startgeolocation(); 
          // this.map.remove();
          // this.loadMap();
          // this.stopTrackingLoop();
        }
        //else{ this.loadMap(); }
    }
    

}

// ionic cordova plugin add https://github.com/mapsplugin/cordova-plugin-googlemaps#multiple_maps --variable API_KEY_FOR_ANDROID="AIzaSyAG5-DA-JzefUKxDd2jern2ftRW8hQGM6s"
// ionic cordova plugin add https://github.com/mapsplugin/cordova-plugin-googlemaps#multiple_maps --variable API_KEY_FOR_IOS="AIzaSyAG5-DA-JzefUKxDd2jern2ftRW8hQGM6s"

// setInterval(() => {
//     this.platform.ready().then(() => {
//         if (!this.map) {
//             this.LoadMap();                    
//         }
//     });
// }, 8000);
//this.startTrackingLoop();

// startTrackingLoop() {
//   this.tracking = setInterval(() => {
//     let templat: number = +localStorage.getItem('lat');
//       this.platform.ready().then(() => {
//         if (templat != null && this.map != null) {
//         this.clearMap();
//         this.map.clear();
//         //this.addmarkers();
//           }
//       });
//     this.stopTrackingLoop();
//   }, 1000);
// }

// LoadMap() {
//     let options = { timeout: 7000, enableHighAccuracy: true };
//     this.geolocation.getCurrentPosition(options).then((resp) => {
//         this.lat = resp.coords.latitude;
//         this.lng = resp.coords.longitude;
//         this.mapElement = document.getElementById('map');
//         let mapOptions: GoogleMapOptions = { camera: { target: { lat: this.lat, lng: this.lng }, zoom: 18, tilt: 30 } };
//         this.map = GoogleMaps.create(this.mapElement, mapOptions);
//         this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
//             console.log('google map is ready!...');
//             //this.startgeolocation();
//             //this.startTrackingPosition();
//         });
//     });
// }


// Map_rray() {
//   if(this.lat && this.lng){
//       var earth = 6378.137;
//       var pi = Math.PI;
//       var m = (1 / ((2 * pi / 360) * earth)) / 1000;
//       var cos = Math.cos;
//       this.tempLocation.forEach(a => {        
//           var maxLat = a.lat + (40 * m);
//           var minLat = a.lat + (-30 * m);        
//           var maxLong = a.lng + (40 * m) / cos(a.lat * (pi / 180));
//           var minLong = a.lng + (-30 * m) / cos(a.lat * (pi / 180));
//           if (this.lat >= minLat && this.lat <= maxLat && this.lng >= minLong && this.lng <= maxLong) {
//               console.log('same loaction');         
//           }
//       });
//   }
// }











